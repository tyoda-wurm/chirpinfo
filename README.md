# ChirpInfo
Simple server mod to display time to next farm tick (chirp) on login and by command.

Commands /chirp, /nextchirp, or /nextcrop will display the current time to next farm tick.

This mod should NOT be used on the same server as the PersistCropTick mod.

# This fork
Currently this fork just adds a version number, and easier compilation.

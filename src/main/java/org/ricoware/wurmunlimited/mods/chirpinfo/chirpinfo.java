package org.ricoware.wurmunlimited.mods.chirpinfo;

import org.gotti.wurmunlimited.modloader.interfaces.PlayerLoginListener;
import org.gotti.wurmunlimited.modloader.interfaces.PlayerMessageListener;
import org.gotti.wurmunlimited.modloader.interfaces.WurmServerMod;

import com.wurmonline.server.creatures.Communicator;
import com.wurmonline.server.players.Player;
import com.wurmonline.server.Server;
import com.wurmonline.server.Servers;

public class chirpinfo implements WurmServerMod, PlayerMessageListener, PlayerLoginListener {

    public static final String version = "ty1.1";

    @Override
    public boolean onPlayerMessage(Communicator communicator, String message) {
        if(message != null) {
            if (message.startsWith("/chirp") || message.startsWith("/nextchirp") || message.equals("/nextcrop")) {
                communicator.sendNormalServerMessage(getNextChirpTime());
                return true;
            }
        }
        return false;
    }

    @Override
    public void onPlayerLogin(Player player) {
        player.getCommunicator().sendSafeServerMessage(getNextChirpTime());
        player.getCommunicator().sendSafeServerMessage("  Type /nextchirp or /nextcrop for update.");
    }

    public static String getNextChirpTime() {
        final long fieldGrowthTime = Servers.localServer.getFieldGrowthTime();

        final long nextChirp = fieldGrowthTime - ((System.currentTimeMillis() - Server.getStartTime()) % fieldGrowthTime);

        return String.format("Crops will need tending in about %s.", Server.getTimeFor(nextChirp)) ;
    }

    @Override
    public String getVersion() {
        return version;
    }
}
